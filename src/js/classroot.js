export class Root {

  constructor (name, avatar, score, verifRoot){
    this.name = name;
    this.avatar = avatar;
    this.score = score;
    this.verifRoot = verifRoot;
    }

 insertRandom(){
  let random = Math.floor(Math.random() * 3) ;
  if (random === 0) {
    document.querySelector('#photo-root').src = './papier.jpg';
    this.verifRoot = "papier";
  }
  else if (random === 1) {
    document.querySelector('#photo-root').src = './caillou.jpg';
    this.verifRoot= "caillou";
 }
 else if (random === 2) {
  document.querySelector('#photo-root').src = './ciseaux.jpg';
  this.verifRoot="ciseaux";
}

}}